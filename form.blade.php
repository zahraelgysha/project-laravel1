<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Sign Up Form</title>
</head>
<body>

	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>

	<form action="/submit" method="POST">
		@csrf
		<label>First name:</label><br><br>
		<input type="text" name="firstname"><br>
		<br>

		<label>Last name:</label><br><br>
		<input type="text" name="lastname"><br>
		<br>

		<p>Gender:</p>
		<input type="radio" name="gender">Male<br>
		<input type="radio" name="gender">Female<br>
		<input type="radio" name="gender">Other<br>

		<p>Nationality</p>
		<select>
			<option name = "bangsa">Indonesia</option>
			<option name = "bangsa">Singapore</option>
			<option name = "bangsa">Malaysia</option>
			<option name = "bangsa">Australia</option>
		</select>

		<p>Language Spoken:</p>
		<input type="checkbox" name="bahasa">Bahasa Indonesia<br>
		<input type="checkbox" name="bahasa">English<br>
		<input type="checkbox" name="bahasa">Other<br>

		<p>Bio:</p>
		<textarea cols="35" rows="10" required=""></textarea>
		<br>
		<button type="Submit">Sign Up</button>

</body>
</html>