<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
	public function index(){
		return view ('Halaman.form');
	}

	public function submit(Request $request){
		$firstname = $request["firstname"];
		$lastname = $request["lastname"];
		$gender = $request["gender"];
		$nationality = $request["bangsa"];
		$language = $request["bahasa"];

		return view('Halaman.welcome', compact('firstname', 'lastname'));
	}
    //
}
?>